class Cart{
    constructor(){
        this.contents = [];
        this.totalAmount = 0;
    }
    addToCart(product,quantity){
        if(product.isActive !== false){
            this.contents.push({product,quantity});
        } else {
            console.log(`${product.name} is currently unavailable.`)
        }
        return this;
    }
    showCartContents(){
        console.log(this.contents);
        return this;
    }
    clearCartContents(){
        this.contents = [];
        this.totalAmount = 0;
        return this;
    }
    computeTotal(){
        let partial = 0;
        this.contents.forEach(product => {
            let total = product.product.price * product.quantity;
            partial += total;
        })
        this.totalAmount = partial;
        return this;
    }
}

class Product{
    constructor(name,price){
        this.name = name;
        this.price = price;
        this.isActive = true;
    }
    archive(){
        this.isActive = false;
        return this;
    }
    avail(){
        this.isActive = true;
        return this;
    }
    updatePrice(newPrice){
        this.price = newPrice;
        return this;
    }
}

class Customer{
    constructor(email){
        this.email = email;
        this.cart = new Cart();
        this.orders = []
    }
    checkOut(){
        if(this.cart.contents.length !== 0){
            this.orders.push({products:this.cart.contents, totalAmount:this.cart.computeTotal().totalAmount});
        }
        this.cart = new Cart();
        return this;
    }
}

let prod1 = new Product("prod1",12.99);
let prod2 = new Product("prod2",24.99);
let cart1 = new Cart();
let cust = new Customer("cust@email.com");